
int main (string[] args) {
	var app = new Gtk.Application ("org.gnome.SidebarExample", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new SidebarExample.Window (app);
		}
		win.present ();
	});

	Hdy.init (ref args);

	return app.run (args);
}
