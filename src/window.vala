
namespace SidebarExample {
	[GtkTemplate (ui = "/org/gnome/SidebarExample/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		[GtkChild]
		private Gtk.Label content_label;
		[GtkChild]
		private Gtk.ListBox list;
		[GtkChild]
		private Hdy.Leaflet leaflet;
		[GtkChild]
		private Gtk.Stack headerbar_stack;
		[GtkChild]
		private Gtk.HeaderBar sub_header_bar;
		[GtkChild]
		private Hdy.Squeezer squeezer;
		[GtkChild]
		private Hdy.ViewSwitcherBar switcher_bar;
		[GtkChild]
		private Gtk.Label title_label;
		[GtkChild]
		private Gtk.Stack stack;

		private int current_section = 0;
		private bool is_narrow;

		public Window (Gtk.Application app) {
			Object (application: app);
			update_ui ();
			on_squeezer_visible_child_changed ();
			on_leaflet_folded_changed ();
			list.set_header_func (list_box_separator_header);
		}

		private void list_box_separator_header (Gtk.ListBoxRow row, Gtk.ListBoxRow? before) {
			if (before != null && row.get_header () == null) {
				var separator = new Gtk.Separator (Gtk.Orientation.HORIZONTAL);
				row.set_header (separator);
			}
		}

		private void update_ui () {
			content_label.label = "Content %d".printf(current_section + 1);
			sub_header_bar.title = "Section %d".printf(current_section + 1);

			var title = "";
			stack.child_get (stack.visible_child, "title", out title, null);
			title_label.label = title;
		}

		private void update_switcher_bar () {
			switcher_bar.reveal = is_narrow && headerbar_stack.visible_child_name == "main";
		}

		[GtkCallback]
		private void on_list_row_activated (Gtk.ListBoxRow? row_item) {
			if (row_item == null)
				return;

			for (int i = 0; i < 3; i++)
				if (list.get_row_at_index (i) == row_item) {
					current_section = i;
					break;
				}

			update_ui ();

			leaflet.visible_child = content_label;
			if (leaflet.folded)
				headerbar_stack.visible_child_name = "subview";

			update_switcher_bar ();
		}

		[GtkCallback]
		private void on_back_clicked () {
			leaflet.visible_child = list;
			headerbar_stack.visible_child_name = "main";
			update_switcher_bar ();
		}

		[GtkCallback]
		private void on_leaflet_folded_changed () {
			list.selection_mode = leaflet.folded ? Gtk.SelectionMode.NONE : Gtk.SelectionMode.BROWSE;
			if (!leaflet.folded)
				list.select_row (list.get_row_at_index (current_section));

			if (leaflet.visible_child != content_label)
				return;

			var visible_child_name = leaflet.folded ? "subview" : "main";

			headerbar_stack.set_visible_child_full (visible_child_name, Gtk.StackTransitionType.NONE);
			update_switcher_bar ();
		}

		[GtkCallback]
		private void on_squeezer_visible_child_changed () {
			is_narrow = squeezer.visible_child == title_label;
			update_switcher_bar ();
		}

		[GtkCallback]
		private void on_stack_visible_child_changed () {
			leaflet.visible_child = list;
			headerbar_stack.visible_child_name = "main";
			update_ui ();
			update_switcher_bar ();
		}
	}
}
